﻿using SocialNeppo.DBContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SocialNeppo.Controllers
{
    public class CriancaAPIController : BaseAPIController
    {
        public HttpResponseMessage Get()
        {
            return ToJson(SocialNeppoDB.TblCriancas.AsEnumerable());
        }

        public HttpResponseMessage Post([FromBody]TblCrianca value)
        {
            SocialNeppoDB.TblCriancas.Add(value);
            return ToJson(SocialNeppoDB.SaveChanges());
        }

        public HttpResponseMessage Put(int id, [FromBody]TblCrianca value)
        {
            SocialNeppoDB.Entry(value).State = EntityState.Modified;
            return ToJson(SocialNeppoDB.SaveChanges());
        }
        public HttpResponseMessage Delete(int id)
        {
            SocialNeppoDB.TblCriancas.Remove(SocialNeppoDB.TblCriancas.FirstOrDefault(x => x.Id_crianca == id));
            return ToJson(SocialNeppoDB.SaveChanges());
        }
    }
}
