﻿using SocialNeppo.DBContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SocialNeppo.Controllers
{
    public class PadrinhoAPIController : BaseAPIController
    {
        public HttpResponseMessage Get()
        {
           return ToJson(SocialNeppoDB.TblPadrinhoes.AsEnumerable());
        }
        // alteraçoes de nome de padrinhos para padrinhoes
        public HttpResponseMessage Post([FromBody]TblPadrinho value)
        {
            SocialNeppoDB.TblPadrinhoes.Add(value);
            return ToJson(SocialNeppoDB.SaveChanges());
        }

        public HttpResponseMessage Put(int id, [FromBody]TblPadrinho value)
        {
            SocialNeppoDB.Entry(value).State = EntityState.Modified;
            return ToJson(SocialNeppoDB.SaveChanges());
        }
        public HttpResponseMessage Delete(int id)
        {
            SocialNeppoDB.TblPadrinhoes.Remove(SocialNeppoDB.TblPadrinhoes.FirstOrDefault(x => x.Id_padrinho == id));
            return ToJson(SocialNeppoDB.SaveChanges());
        }
    }
}
