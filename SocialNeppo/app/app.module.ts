﻿import { NgModule } from '@angular/core'; //, NgModule do núcleo angular
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { HomeComponent } from './components/home.component';

@NgModule({
    imports: [BrowserModule, ReactiveFormsModule, HttpModule, routing], //contém lista de módulos.
    declarations: [AppComponent, HomeComponent], //contém lista de componentes,
    providers: [{ provide: APP_BASE_HREF, useValue: '/' }], //contém a lista de serviços. Vamos adicionar serviço com HTTP operações para 
                                                                        //executar operações de leitura, adição, atualização e exclusão de usuários.
    bootstrap: [AppComponent] //  entrada aos componentes
})

export class AppModule { }